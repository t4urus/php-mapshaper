<?php

namespace T4urus\Mapshaper;

use Symfony\Component\Process\Process;

class Mapshaper {

    public static $_nodeModulePath = null;

    public function run($args)
    {
        $execCmd = (self::$_nodeModulePath) ? realpath(self::$_nodeModulePath . '/.bin/mapshaper') : 'mapshaper';

        $cmd = "{$execCmd} {$args}";
        $output = '$ ' . $cmd . PHP_EOL . PHP_EOL;

        $process = new Process("{$cmd}");
        $process->run(function ($type, $buffer) use (&$output) {
            $output .= $buffer;
        });

        return $output;
    }

    public function setNodeModulePath($path)
    {
        self::$_nodeModulePath = $path;

        return $this;
    }
}